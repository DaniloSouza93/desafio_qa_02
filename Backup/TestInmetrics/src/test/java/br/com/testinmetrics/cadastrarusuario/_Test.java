package br.com.testinmetrics.cadastrarusuario;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import br.com.testinmetrics.utils.Driver;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/br/com/testinmetrics/cadastrarusuario",
				plugin = {"pretty", "html:target/cucumber", "json:target/cucumber.json"},
				strict = false,
				monochrome = true,
				glue = {"br.com.testinmetrics.cadastrarusuario"})

public class _Test {

	@BeforeClass
	public static void iniciaDriver() throws Exception {
		Driver.setUpWebDriver();
	}
	
	@AfterClass
	public static void finalizaDriver() throws Throwable {
		Driver.finaliza();
	}
	
}
